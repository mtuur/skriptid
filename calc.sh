#!/bin/bash
echo "Sisesta esimene number:  "
read a
echo "Sisesta teine number: "
read b
echo "Sisesta soovitud tehe(+ , - , x , /): "
read tehe
#Liitmine
if [ $tehe = "+" ]
then
	vastus=$(expr $a + $b)
	echo "$a + $b = $vastus"
#Lahutamine
elif [ $tehe = "-" ]
then
	vastus=$(expr $a - $b)
	echo "$a - $b = $vastus"
#Jagamine
elif [ $tehe = "/" ]
then
	vastus=$(expr $a / $b)
	echo "$a / $b = $vastus"
#Korrutamine
elif [ $tehe = "x" ]
then 
	vastus=$(expr $a \* $b) 
	echo "$a x $b = $vastus" 
fi
