#!/usr/bin/python

import random

#Loon hangmani 
HANGMAN = (
"""
________
|/  |
|
|
|
|
|
|
""",
"""
________
|/  |
|   O
|
|
|
|
|
""",
"""
________
|/  |
|   O
|   |
|
|
|
|
""",
"""
________
|/  |
|   O
|  \|
|
|
|
|
""",
"""
________
|/  |
|   O
|  \|/
|
|
|
|
""",
"""
________
|/  |
|   O
|  \|/
|   |
|
|
|
""",
"""
________
|/  |
|   O
|  \|/
|   |
|  /
|
|
""",
"""
________
|/  |
|   O
|  \|/
|   |
|  / \
|
|
""")

print
print "Tere tulemast m2ngu hangman!"
print "Sul on 7 k2iku, kui sa selle jooksul s6na 2ra ei arva, puuakse mehike yles"
print "S6nad on teemal: Linuxi k2sud"
print
print "Alustame!"

#Loen sõnade failist sisse suvalise sõna
with open("words.txt") as word_file:
	s6nad = word_file.read().split()
s6na = random.choice(s6nad)

#Defineerin muutujad
k2ik = len(HANGMAN) - 1

pikkus = 0

oige = ""

pakkumised = " "

arvatud = []

while len (oige) != len(s6na):

	print(HANGMAN[pikkus])
    
#Mängu käivitamisel kuvab arvatava sõna asemel _
#Peale  igat õiget pakkumist kuvatakse täht _ asemel 
	for char in s6na:
		if char in pakkumised:
			print char,
		else:
			print "_",

	print
	print
    
#Küsitakse kasutaja sisend ning kontrollitakse, kas see täht on juba pakutud ning sisend ei oleks tühi.
	while True:

		arvamus = raw_input("Paku mingi t2ht: ")

		if arvamus in arvatud:
			print "See t2ht on juba pakutud"

		if arvamus == "":
			print "Sisesta korrektne t2ht!"
            
		else:
			break

	pakkumised += arvamus
#Kuvatakse kui pakutud täht ei ole sõnas ning palju käike on alles. Hangmani pikkus kasvab.		
	if arvamus not in s6na:
		print "___________________"
		print
		print "Vale!"
		pikkus += 1
		k2ik -= 1
		print "Sul on", + k2ik, "k2iku alles"
	arvatud.append(arvamus)

	if arvamus in s6na:
		oige += char
# Kui k2igud otsas, mäng läbi.
	if k2ik == 0:
		print (HANGMAN[pikkus])
		print "GAME OVER!!"
		print ("Arvatav s6na oli: " + s6na)
		exit(0)
#Kui kasutaja arvab terve sõna korraga ära, on ta võitnud.
	if arvamus == s6na:
			print
			print "6ige s6na. Sinu v6it!!"
			exit(0)			
#Kui kasutaja arvab lõpuks sõna ära, on ta võitnud.
	if len(oige) == len(s6na):
		print
		print("Sinu v6it!!")
		print("Arvatav s6na oli: " + s6na)	
		exit(0)