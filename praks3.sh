
#!/bin/bash
(echo "user rss(KiB) vmem(KiB)";
for user in $(users | tr ' ' '\n' | sort -u); 
do
	echo $user $(ps -U $user --no-headers -o rss, vsz \ | awk '{rss+=$1; vmem+=$2}
	 END{print rss" "vmem}')

done | sort -k3
) | column -t
