#!/bin/bash
for USER in $(ps haux | awk '{print $1}' | sort -u)
do
	ps haux | awk -v user=$USER '$1 ~ user { sum += $4 } END { print user, sum; }'
done
