#!/bin/bash

#Kontrollib, kas skript on käivitatud juurkasutajana
if [ $EUID -ne 0 ]
then
	echo 'Pole root!'
	exit 1;
fi

#Kontrollib, kas on ette antud õige arv muutujaid
if [ $# -eq 2 ]; then
	KAUST=$1
	GROUP=$2
	SHARE=$(basename $KAUST)
	else
	if  [ $# -eq 3 ]; then
		KAUST=$1
		GROUP=$2
		SHARE=$3
		else
			echo 'Sisesta ./skirptinimi KAUST GRUPP [SHARE]'
			exit 1;
		fi
fi

#Kontroll, kas sisse on antud täispikk tee. Vajadusel lisatakse
if [ ${KAUST:0:1} == '/' ]; then
	KAUST=$KAUST
else
	KAUST=$(pwd)/$KAUST
fi

#Kontrollib, kas samba on installeeritud
type smbd > /dev/null 2>&1

if [ $? -ne 0 ]; then
	apt-get update && apt-get install samba -y
else
	echo 'Samba on installeeritud'
fi

#Loob vajadusel kausta
mkdir -p $KAUST

#Kontrollin, kas grupp on olemas. Kui mitte, siis loon.
getent group $GROUP > /dev/null || addgroup $GROUP > /dev/null


#Teen backupi
cp /etc/samba/smb.conf /etc/samba/smb.conf.backup

#Lisab tühja rea viimaseks
cat >> /etc/samba/smb.conf.backup << EOF

[$SHARE]
	comment=share folder
	path=$KAUST
	writable=yes
	valid user=@$GROUP
	force group=$GROUP
	browsable=yes
	create mask=0664
	directory mask=0775
EOF

#Kontrollin, kas ridade lisamine õnnestus.
testparm -s /etc/samba/smb.conf.backup
if [ $? -ne 0 ]; then
	echo 'VIGA'
	exit 1;
fi

#Kopeerin õige confi üle
cp /etc/samba/smb.conf.backup /etc/samba/smb.conf

#Teen samba service restardi
/etc/init.d/smbd reload

echo 'Skript on l6ppenud'
