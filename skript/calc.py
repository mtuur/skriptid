#!/usr/bin/python
import math
import sys
import os

#Küsin kasutajalt tehte
print("Vali tehe: liitmine(1), lahutamine(2), korrutamine(3), jagamine(4), astendamine(5), juurimine(6)")
valik = raw_input("")

#kui kasutaja sisetab numbri, mis pole 1-6, antakse teada ja käivitatakse uuesti skript
if valik > '6' or valik < '1':
	print('Sisesta valik 1-6')
	os.execl('cal2.py', '')

#juurimine
if valik == '6':
	num3 = int(input("Sisesta number: "))
	ans = math.sqrt(num3)
	print('Y{} = {}' .format(num3, ans))
	sys.exit(1)

#muutujate loomine
num1 = int(input("Sisesta esimene number: "))
num2 = int(input("Sisesta teine number: "))

#liitmine
if valik == '1':
	ans = num1 + num2
	print('{} + {} = {} ' .format(num1, num2, ans))

#lahutamine
elif valik == '2':
	ans = num1 - num2
	print('{} - {} = {} ' .format(num1, num2, ans))

#korrutamine
elif valik == '3':
	ans = num1 * num2
	print('{} * {} = {} ' .format(num1, num2, ans))

#jagamine
elif valik == '4':
	ans = num1 / num2
	print('{} / {} = {} ' .format(num1, num2, ans))

#astendamine
elif valik == '5':
	ans = num1 ** num2
	print('{} ^ {} = {} ' .format(num1, num2, ans))
