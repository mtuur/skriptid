#!/bin/bash
#Küsin kasutaja sisendeid
echo "Sisesta domeeninimi ja selle ip aadress: "
read domeen ipadd

#Loon muutuja, et reverse tsooni lisada ip tagurpidi
reverse=$(echo "${ipadd}" | awk -F. '{print $4"."$3"."$2"."$1}')

#Kontrollin sisestatud ip aadressi sobivust
if [[ $(ipcalc ${ipadd} | grep 'INVALID') != "" ]]; then
	echo 'ebasobiv ip'
	exit 1;
fi

#Loon muutujad, mis lisab tsooni failides seriali väärtusele +1
serial=$(awk 'NR==7 {print $1; exit}' /etc/bind/db.marta.ee)
sum=$(($serial+1))

serial2=$(awk 'NR==7 {print $1; exit}' /etc/bind/db.1.168.192.in-addr.arpa)
sum2=$(($serial2+1))

#Kontrollin sisendite olemasolu
if grep -qF "$domeen" /etc/bind/db.marta.ee && grep -qF "$ipadd" /etc/bind/db.marta.ee; then
	echo "Domeeninimi ja ip juba eksisteerivad"
	
elif grep -qF "$domeen" /etc/bind/db.marta.ee;then
	echo "Domeeninimi juba eksisteerib"

elif grep -qF "$ipadd" /etc/bind/db.marta.ee;then
	echo "Ip aadress juba eksisteerib"

#Lisan uue kirje faili	
else
	echo $domeen"	IN""	A	" $ipadd >> /etc/bind/db.marta.ee
fi 

#Kontrollin sisendite olemasolu reverse tsooni failis
if grep -qF "$domeen" /etc/bind/db.1.168.192.in-addr.arpa; then
	exit 0;

elif  grep -qF "$ipadd" /etc/bind/db.1.168.192.in-addr.arpa; then
	exit 0;
#Lisan kirje  tsooni faili
else
echo  "$reverse"".in-addr.arpa." "	IN	" "	PTR	" $domeen"." >> /etc/bind/db.1.168.192.in-addr.arpa

fi	
#Asendan seriali numbri tsooni failides
sed -i "7s/.*/			$sum		;serial/g" /etc/bind/db.marta.ee
sed -i "7s/.*/			$sum2		;serial/g" /etc/bind/db.1.168.192.in-addr.arpa

#Teen  bind9 serverile reloadi
service bind9 reload
echo "Domeeninimi ja ip edukalt lisatud"